//
//  FlickrPhotoCell.swift
//  FlickrSearch
//
//  Created by Matt DeBoer on 2/22/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties
    override var isSelected: Bool {
        didSet {
            imageView.layer.borderWidth = isSelected ? 10 : 0
        }
    }
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    //    imageView.layer.borderColor = UIColor.blue
    //    isSelected = false
    }
    
}


