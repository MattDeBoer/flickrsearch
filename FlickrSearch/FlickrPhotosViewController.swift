//
//  FlickrPhotosViewController.swift
//  FlickrSearch
//
//  Created by Matt DeBoer on 2/21/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

final class FlickrPhotosViewController: UICollectionViewController {

    
    // MARK: - Properties
    fileprivate let reuseIdentifier = "FlickrCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    fileprivate let itemsPerRow: CGFloat = 3
    
    
    
    //1  largePhotoIndexPath is an optional that will hold the index path of the tapped photo, if there is one.
    var largePhotoIndexPath: IndexPath? {
        didSet {
            //2  Whenever this property gets updated, the collection view needs to be updated. a didSet property observer is the safest place to manage this.
            var indexPaths = [IndexPath]()
            if let largePhotoIndexPath = largePhotoIndexPath {
                indexPaths.append(largePhotoIndexPath as IndexPath)
            }
            if let oldValue = oldValue {
                indexPaths.append(oldValue as IndexPath)
            }
            //3  performBatchUpdates will animate any changes to the collection view performed inside the block. You want it to reload the affected cells.
            collectionView?.performBatchUpdates({
                self.collectionView?.reloadItems(at: indexPaths)
            }) { completed in
                //4  Once the animated update has finished, it’s a nice touch to scroll the enlarged cell to the middle of the screen
                if let largePhotoIndexPath = self.largePhotoIndexPath {
                    self.collectionView?.scrollToItem(
                        at: largePhotoIndexPath as IndexPath,
                        at: .centeredVertically,
                        animated: true)
                }
            }
        }
    }
    
 
    fileprivate var searches = [FlickrSearchResults]()
    fileprivate let flickr = Flickr()
    

}
// MARK: - UICollectionViewDataSource
extension FlickrPhotosViewController {
    //1  only one count per search, so number of sections is the count of searches array
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return searches.count
        
    }
    
    //2 number of lines in a section is the count of the serach results array from flickrsearch object
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return searches[section].searchResults.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        //1 indicates which sort of suppleentary view is being asked for
        switch kind {
        //2 supplementary view belongs to flow layout, used for header
        case UICollectionElementKindSectionHeader:
            //3 header view is dequeued using the identifier added in storyboard
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                    withReuseIdentifier: "FlickrPhotoHeaderView",
                    for: indexPath) as! FlickrPhotoHeaderView
        headerView.label.text = searches[(indexPath as NSIndexPath).section].searchTerm
        return headerView
        default:
            //4  assert is used to make it clear that only a header view will be asked for
            assert(false, "Unexpected element kind")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier, for: indexPath) as! FlickrPhotoCell
        let flickrPhoto = photoForIndexPath(indexPath: indexPath)
        
        //1  Always stop the activity spinner – you could be reusing a cell that was previously loading an image
        cell.activityIndicator.stopAnimating()
        
        //2 This part is as before – if you’re not looking at the large photo, just set the thumbnail and return
        guard indexPath == largePhotoIndexPath else {
            cell.imageView.image = flickrPhoto.thumbnail
            return cell
        }
        
        //3 If the large image is already loaded, set it and return
        guard flickrPhoto.largeImage == nil else {
            cell.imageView.image = flickrPhoto.largeImage
            return cell
        }
        
        //4 By this point, you want the large image, but it doesn’t exist yet. Set the thumbnail image and start the spinner going. The thumbnail will stretch until the download is complete
        cell.imageView.image = flickrPhoto.thumbnail
        cell.activityIndicator.startAnimating()
        
        //5 Ask for the large image from Flickr. This loads the image asynchronously and has a completion block
        flickrPhoto.loadLargeImage { loadedFlickrPhoto, error in
            
            //6 he load has finished, so stop the spinner
            cell.activityIndicator.stopAnimating()
            
            //7  If there was an error or no photo was loaded, there’s not much you can do.
            guard loadedFlickrPhoto.largeImage != nil && error == nil else {
                return
            }
            
            //8 Check that the large photo index path hasn’t changed while the download was happening, and retrieve whatever cell is currently in use for that index path
            if let cell = collectionView.cellForItem(at: indexPath) as? FlickrPhotoCell
                , indexPath == self.largePhotoIndexPath  {
                cell.imageView.image = loadedFlickrPhoto.largeImage
            }
        }
        
        return cell
   }
}
// MARK: - UICollectionViewDelegate
extension FlickrPhotosViewController {
    
    override func collectionView(_ collectionView: UICollectionView,
                                 shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        largePhotoIndexPath = largePhotoIndexPath == indexPath ? nil : indexPath
        return false
    }
}

extension FlickrPhotosViewController : UICollectionViewDelegateFlowLayout {
    //1  tells the layout size of a given cell
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                       sizeForItemAt indexPath: IndexPath) -> CGSize {
        // New code
    if indexPath == largePhotoIndexPath {
            let flickrPhoto = photoForIndexPath(indexPath: indexPath)
            var size = collectionView.bounds.size
            size.height -= topLayoutGuide.length
            size.height -= (sectionInsets.top + sectionInsets.right)
            size.width -= (sectionInsets.left + sectionInsets.right)
            return flickrPhoto.sizeToFillWidthOfSize(size)
        }
        //2 total amount of space taken up by padding, returns the size as a square
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3  returns the spacing between the cells, headers, and footers, constant is used to store the value
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4  controls the spacing between each line in the layout, you want it to match the padding at the left and right
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}


    extension FlickrPhotosViewController : UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // 1 adds the activity view flickr wrap class searches flickr for matching items
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            textField.addSubview(activityIndicator)
            activityIndicator.frame = textField.bounds
            activityIndicator.startAnimating()
            
            flickr.searchFlickrForTerm(textField.text!) {
                results, error in
                
                
                activityIndicator.removeFromSuperview()
                
                
                if let error = error {
                    // 2  log any errors to consolse with searching
                    print("Error searching : \(error)")
                    return
                }
                
                if let results = results {
                    // 3  results get logged and added to the front of the searches array
                    print("Found \(results.searchResults.count) matching \(results.searchTerm)")
                    self.searches.insert(results, at: 0)
                    
                    // 4 this reloades the data to refresh the UI similiar to the tableview use
                    self.collectionView?.reloadData()
                }
            }
            
            textField.text = nil
            textField.resignFirstResponder()
            return true
        }
    }


    // MARK: - Private
    private extension FlickrPhotosViewController {
        func photoForIndexPath(indexPath: IndexPath) -> FlickrPhoto {
            return searches[(indexPath as IndexPath).section].searchResults[(indexPath as IndexPath).row]
        }
    }

    

