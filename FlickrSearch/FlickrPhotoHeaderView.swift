//
//  FlickrPhotoHeaderView.swift
//  FlickrSearch
//
//  Created by Matt DeBoer on 2/22/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit

class FlickrPhotoHeaderView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!
    
}
